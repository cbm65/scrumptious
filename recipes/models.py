from django.db import models
from datetime import timedelta
from django.conf import settings
# Create your models here.
class Recipe(models.Model):

    title = models.CharField(max_length=200)
    picture = models.URLField()
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    cook_time = models.DurationField(default=timedelta)
    rating = models.SmallIntegerField(default=0)

    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name = "recipes",
        on_delete=models.CASCADE,
        null=True,

    )

    def __str__(self):
        return self.title


class RecipeStep(models.Model):
    instruction = models.TextField()
    order = models.PositiveIntegerField()
    recipe = models.ForeignKey(
        Recipe,
        related_name="steps",
        on_delete=models.CASCADE,
    )

    def recipe_title(self):
        return self.recipe.title

    #a way to tell Django about how we want it to interact with this object
    class Meta:
        #defines default ordering for when we get the RecipeStep objects
        ordering = ["order"]

class Ingredient(models.Model):
    amount = models.CharField(max_length=100)
    food_item = models.CharField(max_length=100)
    recipe = models.ForeignKey(
        Recipe,
        related_name="ingredients",
        on_delete=models.CASCADE,
    )

    def recipe_title(self):
        return self.recipe.title

    #a way to tell Django about how we want it to interact with this object
    class Meta:
        #defines default ordering for when we get the RecipeStep objects
        ordering = ["food_item"]
