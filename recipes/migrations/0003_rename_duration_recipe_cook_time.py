# Generated by Django 4.1.3 on 2022-11-28 19:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0002_recipe_duration_recipe_rating'),
    ]

    operations = [
        migrations.RenameField(
            model_name='recipe',
            old_name='duration',
            new_name='cook_time',
        ),
    ]
